<!-- Load up up our variable -->
<?php 
	$items = $variables['items'];
	
?>

<!-- Main Nav -->
<div class = 'main-nav'>
	<?php
		foreach($items as $key => $value) {
			if ($key == 'documents') {
				echo "<div class = 'main-link'>";
				echo $value;
				echo "</div>";
			}
			else {
				echo "<div class = 'main-link'>";
				echo $value . ' | ';
				echo "</div>";
			}
		}
	?>
</div>
<?php
/**
 *  $variables is the signature of the function
 */

function subtheme_sample_breadcrumb($variables) {
	$links = array();
	$path = '';


	// Get URL arguments
	$arguments = explode('/', request_uri());

	// Remove empty values
	foreach ($arguments as $key => $value) {
		if (empty($value)) {
			unset($arguments[$key]);
		}
	}

	$arguments = array_values($arguments);

	// Add 'Home' link
	$links[] = l(t('Home'), '<front>');

	// Add other links
	if (!empty($arguments)) {
		foreach ($arguments as $key => $value) {
      		// Don't make last breadcrumb a link
			if ($key == (count($arguments) - 1)) {
				$links[] = drupal_get_title();
			} 
			else {
				if(!strcmp($value,"?=node") or !strcmp($value,"?=board")) {
					if (!empty($path)) {
						$path .= '/'. $value;
					}
					else {
						$path .= $value;
					}
					$links[] = l(drupal_ucfirst($value), $path);
				}
			}
		}
	}


	// Set custom breadcrumbs
	drupal_set_breadcrumb($links);


	// Get custom breadcrumbs
	$breadcrumb = drupal_get_breadcrumb();


	// Hide breadcrumbs if only 'Home' exists
	if (count($breadcrumb) > 1) {
		return '<div class="breadcrumb">'. implode('<span> &#9656; </span>', $breadcrumb) .'</div>';
	}

	return '';
}		